package base;

/**
 * 
 * @author Chiara
 *
 * Interface implemented by all objects that will move 
 */
public interface Animation {
    
    public void animation();

}
